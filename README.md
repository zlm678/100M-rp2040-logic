# 100M树莓派RP20402逻辑分析仪

#### 介绍
100M树莓派RP20402逻辑分析仪(注意PCB没有做等长布线),有两个开源方案，第一个使用的是PluseView软件，第二个使用logicanalyzer
。区别是logicanalyzer，全英文，功能不多，对新手不友好，但是他可以达到100M的采样率（即每秒1亿次采样），目前支持分析这几种协议I2C,SPI,UART（但可以自己扩展）
具体区别请看语雀笔记：https://www.yuque.com/yuqueyonghuoaufph/ym9r6r/xwbv51usk23cu6tn?singleDoc# 《树莓派100MHZ逻辑分析仪》
#### 图片
![原理图](https://foruda.gitee.com/images/1699090259864472338/b1366aa8_7427755.jpeg "原理图.jpg")
![PCB](https://foruda.gitee.com/images/1699090297823101449/46df0e27_7427755.jpeg "PCB3D图.jpg")


#### 参考开源

[24 channel, 100Msps logic analyzer hardware and software](https://github.com/gusmanb/logicanalyzer)
[ula](https://github.com/dotcypress/ula.git)
